# standard imports
import logging
from http.client import parse_headers
import email
import sys
import urllib
from urllib.request import urlopen, urlunparse, Request
import base64
import re
import hashlib

# external imports
from http_hoba_auth import Hoba

# local imports
from usumbufu.client.base import ClientSession
from usumbufu.client import get_header_parts

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()

host = 'localhost'
port = 5555
origin = 'http://' + host + ':' + str(port)

url = urlunparse(('http', host + ':' + str(port), '', '', '', None,))


# XORSigner is a demo signer that renders a "signature" by xoring the hoba signature material with the user's "key"
class XORSignerSession(ClientSession):

    # applies the "signature"
    def sign(self, key, message):
        signature = bytearray()
        for i, b in enumerate(message):
            signature.append(b^key[i])
        return signature


    # sha256 hash the secret to create a fitting digest-length string to xor
    def to_message(self, s):
        h = hashlib.new('sha256') # RSA-SHA256 -> hoba algo 0
        h.update(s.encode('utf-8'))
        z = h.digest()
        logg.debug('sign material {}'.format(z.hex()))
        return z


    # Triggered by parent class ClientSession on a 401 with HOBA auth method
    def process_auth_challenge(self, header, encoding=None, method=None):

        # we only know HOBA
        if method != 'HOBA':
            raise RuntimeError('unhandled method {}'.format(method))

        # retrieve the challenge
        ap = get_header_parts(header)
        challenge = base64.b64decode(ap['challenge'])

        # build the hoba signature material
        hoba = Hoba(url, ap.get('realm', ''))
        hoba.challenge = challenge
        hoba.nonce = b'\xee' * 32
        hoba.kid = b'\xff' * 32             # user key, a.k.a. just some random stuff that doubles as private and public key here

        # create message and apply the signature
        tbs = hoba.to_be_signed()
        msg = self.to_message(tbs)
        hoba.signature = self.sign(hoba.kid, msg) 

        return hoba


# Construct an urllib handler that can take care of our 401 with HOBA
handler = XORSignerSession(origin)
opener = urllib.request.build_opener(handler)

# The opener will automatically generate the HOBA response within the 401 handler code, and in turn call the XORSignerSession.process_auth_challenge.
req = Request(url)
r = opener.open(req)
print(r.read().decode('utf-8'))
