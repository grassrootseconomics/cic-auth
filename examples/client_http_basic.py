# standard imports
import sys
import ssl
import urllib
from urllib.request import urlopen, Request
import base64

username = 'foo'
password = 'bar'

if __name__ == '__main__':

    req = Request('https://localhost:5555')
    up = '{}:{}'.format(username, password)
    bup = base64.b64encode(up.encode('utf-8'))
    req.add_header('Authorization', 'Basic {}'.format(bup.decode('utf-8')))

    sctx = ssl.SSLContext()
    sctx.verify_mode = ssl.CERT_NONE # ok for testing

    u = urllib.request.urlopen(
            req,        
            timeout=1,
            context=sctx,
            )

    print(u.read().decode('utf-8'))
