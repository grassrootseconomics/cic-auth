# standard imports
import os
import logging
from http.client import parse_headers
import email
import sys
import urllib
from urllib.request import urlopen, urlunparse, Request
import base64
import re
import hashlib
import time

# external imports
from http_hoba_auth import Hoba

# local imports
from usumbufu.client.pgp import PGPClientSession
from usumbufu.client.bearer import BearerClientSession
from usumbufu.client.base import (
        ClientSession,
        BaseTokenStore,
        )
from usumbufu.client import get_header_parts

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()

host = 'localhost'
port = 5555
origin = 'http://' + host + ':' + str(port)

script_dir = os.path.dirname(os.path.realpath(__file__))
testdata_dir = os.path.join(script_dir, '..', 'tests', 'testdata')
pgp_dir = os.path.join(testdata_dir, 'pgp')

url = urlunparse(('http', host + ':' + str(port), '', '', '', None,))

f = open(os.path.join(pgp_dir, 'merman.priv.asc'), 'r')
pk_export = f.read()
f.close()

token_store = BaseTokenStore()
session = ClientSession(origin, token_store=token_store)
handler = PGPClientSession(origin, private_key=pk_export, passphrase='merman', gpg_dir=pgp_dir)
session.add_subhandler(handler)

# Construct an urllib handler that can take care of our 401 with HOBA
opener = urllib.request.build_opener(session)

# The opener will automatically generate the HOBA response within the 401 handler code, and in turn call the XORSignerSession.process_auth_challenge.
req = Request(url)
r = opener.open(req)
print(r.headers)

#session_token = r.getheader('Token')
session = ClientSession(origin, token_store=token_store)
handler = BearerClientSession(origin, token_store=token_store)
session.add_subhandler(handler)
opener = urllib.request.build_opener(session)
req = Request(url)
r = opener.open(req)
print(r.headers)

time.sleep(1)
req = Request(url)
r = opener.open(req)
print(r.headers)
