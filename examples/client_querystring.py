# standard imports
import sys
import ssl
import urllib
from urllib.request import urlopen

username = 'bar'
password = 'foo'

if __name__ == '__main__':

    sctx = ssl.SSLContext()
    sctx.verify_mode = ssl.CERT_NONE # ok for testing

    u = urllib.request.urlopen(
            'https://localhost:5555?username={}&password={}'.format(username, password),
            timeout=1,
            context=sctx,
            )

    print(u.read().decode('utf-8'))
