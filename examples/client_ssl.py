# standard imports
import logging
import os
import sys
import ssl
import urllib
from urllib.request import urlopen

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()

script_dir = os.path.dirname(os.path.realpath(__file__))

client_cert_path = os.path.join(script_dir, 'client.crt')
client_key_path = os.path.join(script_dir, 'client.key')

if __name__ == '__main__':

    sctx = ssl.SSLContext()
    sctx.load_cert_chain(
        client_cert_path,
        client_key_path,
        )
    sctx.verify_mode = ssl.CERT_NONE # ok for testing

    u = urllib.request.urlopen(
            'https://localhost:5555',
            timeout=1,
            context=sctx,
            )

    print(u.read().decode('utf-8'))
