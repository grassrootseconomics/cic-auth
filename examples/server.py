# standard imports
import logging
from urllib import request
import base64

# local imports
from usumbufu.adapters.uwsgi import UWSGIAdapter, UWSGISSLClient, UWSGIQueryString, UWSGIHTTPAuthorization
from usumbufu.error import AuthenticationError
from usumbufu.retrieve import Fetcher

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()


class ExampleFetcher(Fetcher):

    user_registry = {
        'ssl': {
            '1677d274ad170be6b818b1c3d6c8a618cc23e6e4e8849bee60157f67136a37bf': base64.b64encode(b'<SSL USER AUTH INFO>'),
            },
        'http': {
            # user foo, pass bar
            'a765a8beaa9d561d4c5cbed29d8f4e30870297fdfa9cb7d6e9848a95fec9f937': base64.b64encode(b'<HTTP USER AUTH INFO>'), 
            },
        'qs': {
            # user bar, pass foo
            '5a25bbe23cfa513f365e776f8c73a93739085c9f6c94721984a647998d0a696c': base64.b64encode(b'<QUERYSTRING USER AUTH INFO>'),
            },

        }


    def __init__(self, typ):
        self.typ = typ


    def __decrypt(self, auth_info_ciphertext):
        return base64.b64decode(auth_info_ciphertext).decode('utf-8')


    def load(self, ip, auth_digest):
        auth_digest_hex = auth_digest.hex()
        logg.debug('attempting fetcher type {} load digest {}'.format(self.typ, auth_digest_hex))
        auth_info_ciphertext = self.user_registry[self.typ].get(auth_digest_hex)
        if auth_info_ciphertext == None:
            raise AuthenticationError(auth_digest_hex)
        auth_info_plaintext = self.__decrypt(auth_info_ciphertext)
        return (auth_info_plaintext, auth_digest)

ssl_fetcher = ExampleFetcher('ssl')
http_fetcher = ExampleFetcher('http')
qs_fetcher = ExampleFetcher('qs')

# uwsgi application
def application(env, start_response):

    headers = []
    content = b''

    # The auth vector that will try one auth method after another, returning at the first successful one
    authenticator = UWSGIAdapter()

    # ssl client auth
    ssl_authenticator = UWSGISSLClient(ssl_fetcher, env)
    authenticator.register(ssl_authenticator)
    authenticator.activate(ssl_authenticator.component_id)

    # http authorization auth
    http_authenticator = UWSGIHTTPAuthorization(http_fetcher, env, 'GE')
    authenticator.register(http_authenticator)
    authenticator.activate(http_authenticator.component_id)

    # naive queryparam auth
    authenticator.register(UWSGIQueryString(qs_fetcher, env, ['127.0.0.1']))
    authenticator.activate(UWSGIQueryString.component_id)

    result = authenticator.check()
    if not result:
        start_response('401 Unauthorized', headers)
        return [content]

    content = """Type: {}
Digest: {}
Auth info: {}
""".format(
        result[0],
        result[1].hex(),
        result[2],
        ).encode('utf-8')

    headers.append(('Content-Length', str(len(content))),)
    headers.append(('Content-Type', 'text/plain, charset=UTF-8'),)
    start_response('200 OK', headers)

    return [content]
