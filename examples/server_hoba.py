# standard imports
import logging
import os
import datetime

# external imports
from http_hoba_auth import hoba_auth_request_string
from http_token_auth import SessionStore
        
# local imports
from usumbufu.challenge import Challenger
from usumbufu.filter.sha256 import SHA256Filter
from usumbufu.filter.hoba import HobaFilter
from usumbufu.filter.fetcher import FetcherFilter
from usumbufu.filter.session import SessionFilter
from usumbufu.filter import Filter
from usumbufu.retrieve import Retriever
from usumbufu.adapters.uwsgi import UWSGIHTTPAuthorization
from usumbufu.adapters.uwsgi import UWSGIAdapter

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()

origin_host = 'localhost'
realm = 'foorealm'
origin_port = '5555'
origin_string = 'http://' + origin_host + ':' + origin_port
cipher_id = '00'


# This is the client user's "key" in the example. It's just some random stuff
user_key = b'\xff' * 32


# This is the "ACL" document format that will be returned upon succesful validation of a challenge signature
# it will need to be interpreted by this web server
user_registry = {
        user_key: 'foo:4,bar:2',
        }


# This is a key-value mapping to a user name for the authentication key
# In real decentralized life this could come from the same source as the ACL or a different one
user_name_registry = {
        user_key.hex(): 'King Randor',
        }


# Our web server will have to make sense of the ACL data being returned on a successful authentication.
# the ecuth Session object that encapsulates an authenticated session expects the ACL object to implement the read(v) and write(v) to check whether user is autorized to read or write access on v, respectively
# it also expects the val() method to get the raw access value for v
class MockAcl:

    read_bit = 4
    write_bit = 2

    def __init__(self, v):
        self.readers = []
        self.writers = []
        items = v.split(',')
        for item in items:
            o = item.split(':')
            access_bits = int(o[1])
            if access_bits & self.read_bit > 0:
                self.readers.append(o[0])
            if access_bits & self.write_bit > 0:
                self.writers.append(o[0])


    def read(self, v):
        return v in self.readers


    def write(self, v):
        return v in self.writers


    def val(self, v):
        r = 0
        if v in self.readers:
            r |= self.read_bit
        if v in self.writers:
            r |= self.write_bit
        return r


# Below here we set up ecuth to handle HOBA requests

# this is where we translate the ACL. It is the last method to be called in the authentication pipeline.
class MockAclFilter(Filter):

    default_name = 'mock acl filter'

    def decode(self, requester_ip, v, signature=None, identity=None):
        return (MockAcl(v), signature, identity)


# the resolver is required by euth.ChallengeRetriever object, and is responsible for checking the signature against the challenge during the validation process, and returning the key recovered from it.
# If it cannot make sense of the signature, it should return None
# Note that this "signature" is no more than just xor of the client
class MockSignatureResolver(Filter):

    default_name = 'mock signature resolver'

    def decode(self, requester_ip, hashed_challenge, signature=None, identity=None):
        logg.debug('mock signature resolver checking signature {} for challenge {}'.format(signature.hex(), hashed_challenge.hex()))
        check_signature = bytearray()
        for i, b in enumerate(hashed_challenge):
            check_signature.append(b^user_key[i])

        if check_signature != signature:
            logg.error('signature incorrect {} {}'.format(signature, check_signature))
            return None

        logg.info('signature ok; key {}'.format(user_key))
        return (user_key, signature, user_key)


# The Retriever is at the heart of the setup.
# It will run all the decoding steps from all the filters, ultimately resolving to a auth resource (typically ACL) and an identity
challenger = Challenger()

# parse a hoba auth string to key, signature, nonce etc, to the "to be signed" format
hoba_filter = HobaFilter(origin_string, realm, challenger, alg=cipher_id)

# hash the "to be signed" format. this yields the exact message the client has signed
hasher_filter = SHA256Filter()

# validate the signature from the message (a non-mock resolver may also verify and/or recover the signing key here)
signature_resolver_filter = MockSignatureResolver()

# set a session token for the identity, if not yet set
# the session store can be used outside the pipeline to get session info (like the auth token for a Token header response)
session_store = SessionStore()
session_filter = SessionFilter(session_store)

# fetch the acl records from whatever resource holds them. since the Fetcher interface uses get(), we will put a dict here :)
fetcher_filter = FetcherFilter(user_registry)

# parse the fetched acl into a structured object that's easy to work with in business logic
acl_filter = MockAclFilter()

# the decoders will be run in SEQUENCE. let's wire them up, like so:
hoba_retriever = Retriever()
hoba_retriever.add_decoder(hoba_filter)
hoba_retriever.add_decoder(hasher_filter)
hoba_retriever.add_decoder(signature_resolver_filter)
hoba_retriever.add_decoder(session_filter)
hoba_retriever.add_decoder(fetcher_filter)
hoba_retriever.add_decoder(acl_filter)


# Below here is the runtime code for the UWSGI application
# Most important to notice here is that the ChallengeRetriever is being passed to the UWSGIHTTPAuthorization object. This object can identify a HOBA request, and will attempt to validate the HOBA auth string using ChallengeRetriever.
# cic_eth.Auth.check() (overloaded) will attempt to FETCH the ACL using the key (if any) resulting from the validation
def do_auth(env):
    authenticator = UWSGIAdapter()
    http_authenticator = UWSGIHTTPAuthorization(hoba_retriever, env, realm, origin=origin_string)
    authenticator.register(http_authenticator)
    authenticator.activate(http_authenticator.component_id)
    return authenticator.check()
    

# And to conclude, vanilla UWSGI stuff
def application(env, start_response):
    headers = []
    result = do_auth(env)
    if result == None:
        if env.get('HTTP_AUTHORIZATION') != None:
            start_response('403 failed miserably', headers)
            return [b'']
        (challenge, expire) = challenger.request(env['REMOTE_ADDR'])
        headers.append(('WWW-Authenticate', hoba_auth_request_string(challenge, expire.timestamp(), realm=realm)),)
        start_response('401 authenticate or I will SCREAM_SNAKE_CASE at you', headers)
        return [b'']

    # name the successful auth result parts 
    auth_method_component_id = result[0]
    auth_string = result[1]
    auth_resource = result[2]
    auth_identity = result[3]
   
    session = session_store.get(auth_identity)
    logg.debug(f'session token: {session.auth}')

    headers.append()

    # build a sensible response
    auth_user_key = session.identity.hex()
    auth_user_name = user_name_registry[auth_user_key]
    s = """Greetings, {}!

You've used {} with your key {} to authenticate. Good show!

Your session with us is stored under the authentication identifier {}.

Here's a breakdown of what you are authorized to do here:

resource 'foo':
    - read-access: {}
    - write-access: {}
resource 'bar':
    - read-access: {}
    - write-access: {}

your session token expires at {}, and if this were oauth, you could retrieve new session tokens until {}
""".format(
        auth_user_name,
        auth_method_component_id,
        auth_user_key,
        session.auth.hex(),
        auth_resource.read('foo'),
        auth_resource.write('foo'),
        auth_resource.read('bar'),
        auth_resource.write('bar'),
        datetime.datetime.fromtimestamp(session.auth_expire),
        datetime.datetime.fromtimestamp(session.refresh_expire),
    )

    start_response('200 OK', headers)
    return [s.encode('utf-8')]
