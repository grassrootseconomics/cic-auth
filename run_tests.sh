#!/bin/bash

set -a
set -e
set -x
default_pythonpath=$PYTHONPATH:.
export PYTHONPATH=${default_pythonpath:-.}
>&2 echo using pythonpath $PYTHONPATH
pytest -x --cov=usumbufu --cov-fail-under=90 --cov-report term-missing ./tests 
set +x
set +e
set +a
