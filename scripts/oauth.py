# standard imports
import os
import logging
from urllib.parse import parse_qs
import uuid
import re
import time
import json

# third-party imports
#from jinja2 import Template

# local imports
from cic_auth.adapters.uwsgi import UWSGIAdapter, UWSGISSLClient, UWSGIHTTPAuthorization
from cic_auth.ext.oauth import OauthClientAuthorization
from cic_auth.error import CredentialsError
from cic_auth.error import AuthenticationError
from cic_auth.fetcher import Fetcher
from ecuth.session import Session
from ecuth.acl.yaml import YAMLAcl

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()

root_dir = os.path.dirname(__file__)
static_dir = os.path.join(root_dir, 'static')

DEFAULT_AUTH_EXPIRE = 60
DEFAULT_REFRESH_EXPIRE = 60 * 60 * 24 * 30

tmp_client_registry = [
        '2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae', # "foo"
        ]

tmp_user_registry = {
        '44c9b278729d7b8564ffddfbb2c5d7679e4428af425ed1ae5d5334e159997826': """level: 9
items:
    foo: 4
    bar: 2
""",
        'a765a8beaa9d561d4c5cbed29d8f4e30870297fdfa9cb7d6e9848a95fec9f937': """level: 0
items:
    baz: 4
    xyzzy: 2
""",
}


class ExampleSession(Session):
    pass


# TODO: refactor as extension of ExampleSession
class OauthToken:

    client_nonce_index = {}
    client_code_index = {}
    client_user_index = {}
    session = {}

    def __init__(self, client_id, client_uri=None):
        self.created = time.time()
        self.client_id = client_id
        self.client_nonce = uuid.uuid4().hex
        self.client_uri = client_uri
        self.client_state = None
        self.client_code = None
        self.client_user = None


    def save(self, state):
        self.client_state = state
        k = '{}:{}'.format(self.client_id, self.client_nonce)
        OauthToken.client_nonce_index[k] = self
        return k


    def generate_code(self):
        self.client_code = uuid.uuid4().hex
        OauthToken.client_code_index[self.client_code] = self
        k = '{}:{}'.format(self.client_id, self.client_nonce)
        del OauthToken.client_nonce_index[k]
        return self.client_code


    def release(self, client_code):
        pass


    @staticmethod
    def get_by_nonce(client_id, client_nonce):
        k = '{}:{}'.format(client_id, client_nonce)
        return OauthToken.client_nonce_index.get(k)


    @staticmethod
    def get_by_code(client_code):
        return OauthToken.client_code_index.get(client_code)



def validate_client_id(s):
    logg.debug('client_id {}'.format(s))
    return s


def validate_response_type(s):
    logg.debug('response_type {}'.format(s))
    if s != 'code':
        raise ValueError


def validate_state(s):
    return s


class ExampleAuthorizeHandler(Fetcher):

    def load(self, identity):
        identity_hex = identity.hex()
        logg.debug('update client request {}'.format(identity_hex))
        if identity_hex not in tmp_client_registry:
            return None
        return identity_hex

    def authenticator(self, env):
        client_id = None
        client_state = None
        client_uri = None

        q = parse_qs(env['QUERY_STRING'])

        try:
            client_id = validate_client_id(q['client_id'][0])
        except KeyError:
            raise CredentialsError('Missing client_id')
        except ValueError:
            raise CredentialsError('Invalid client_id')

        try:
            validate_response_type(q['response_type'][0])
        except KeyError:
            raise CredentialsError('Missing response_type')
        except ValueError:
            raise CredentialsError('Invalid response_type')

        try:
            state = q['state']
        except KeyError:
            raise CredentialsError('Missing client state')

        if q.get('redirect_uri') != None:
            client_uri = q.get('redirect_uri')

        #authenticator = UWSGIAdapter(None, env, 'GE', client_id)
        #fetcher = ExampleAuthorizeFetcher()
        authenticator = UWSGIAdapter()
        oauth_auth = OauthClientAuthorization(self.load, client_id)
        authenticator.register(oauth_auth)
        authenticator.activate(oauth_auth.component_id)
        return authenticator



class ExampleLoginHandler(Fetcher):

    sessions = {}

    def load(self, identity):
        logg.debug('fetcher login check {}'.format(identity.hex()))
        auth_info = None
        try:
            auth_info = tmp_user_registry[identity.hex()]
        except:
            raise AuthenticationError()
        session = ExampleSession(identity, YAMLAcl)
        ExampleLoginHandler.sessions[identity] = session
        return auth_info


    def authenticator(self, env):
        authenticator = UWSGIAdapter()
        http_authenticator = UWSGIHTTPAuthorization(self, env, 'GE')
        authenticator.register(http_authenticator)
        authenticator.activate(http_authenticator.component_id)
        return authenticator


class ExampleTokenHandler(Fetcher):

    sessions = {}
    session_auths = {}

    def load(self, identity):
        logg.debug('fetcher token check {}'.format(identity.hex()))
        auth_info = None
        try:
            auth_info = tmp_user_registry[identity.hex()]
        except:
            logg.debug('not user {}'.format(identity))
        session = ExampleSession(identity, YAMLAcl)
        ExampleTokenHandler.sessions[identity] = session
        ExampleTokenHandler.session_auths[session.auth] = session
        return auth_info


    def authenticator(self, env):
        authenticator = UWSGIAdapter()
        http_authenticator = UWSGIHTTPAuthorization(self, env, 'GE')
        authenticator.register(http_authenticator)
        authenticator.activate(http_authenticator.component_id)
        return authenticator


class ExampleValidateHandler(Fetcher):

    def load(self, identity):
        session = ExampleTokenHandler.session_auths[identity]
        return ExampleLoginHandler().load(session.identity)


    def authenticator(self, env):
        fetcher = ExampleValidateHandler() #DigestFetcher(fetcher_validate)
        authenticator = UWSGIAdapter()
        http_authenticator = UWSGIHTTPAuthorization(self, env, 'GE')
        authenticator.register(http_authenticator)
        authenticator.activate(http_authenticator.component_id)
        return authenticator
    

def do_auth(env):
    authenticator = None
    if env['PATH_INFO'] == '/authorize':
        h = ExampleAuthorizeHandler()
        authenticator = h.authenticator(env) #handle_authorize(env)
    elif env['PATH_INFO'] == '/login':
        h = ExampleLoginHandler()
        authenticator = h.authenticator(env) #handle_authorize(env)
    elif env['PATH_INFO'] == '/token':
        h = ExampleTokenHandler()
        authenticator = h.authenticator(env) #handle_authorize(env)
    elif env['PATH_INFO'] == '/validate/acl':
        h = ExampleValidateHandler()
        authenticator = h.authenticator(env) #handle_authorize(env)
    else:
        raise FileNotFoundError()
    return authenticator


def do_content(auth_result, env):
    status = '200 OK'
    headers = []
    content = b''

    if env['PATH_INFO'] == '/authorize':
        q = parse_qs(env['QUERY_STRING']) # TODO: don't parse querystring twice
        uri = q.get('redirect_uri')
        if uri != None:
            uri = uri[0]
        client = OauthToken(q['client_id'][0], uri)
        client.save(q['state'][0])
        headers.append(('Location', '/login'),)
        headers.append(('Set-Cookie', '_login_{}={}'.format(client.client_nonce, client.client_id),))
        status = '302 Found'

    elif env['PATH_INFO'] == '/login':
        client_id = None
        client_nonce = None
        re_client = r'^_login_(.*)=(.*)$'
        m = re.match(re_client, env['HTTP_COOKIE'])
        if m != None:
            ValueError('missing client context')

        client_id = m[2]
        client_nonce = m[1]
        if client_id == None:
            raise ValueError('missing client_id')
        elif client_nonce == None:
            raise ValueError('missing client_nonce')

        t = OauthToken.get_by_nonce(client_id, client_nonce)
        if t == None:
            raise KeyError('invalid client context')

        t.generate_code()
        uri = ''
        if t.client_uri != None:
            uri = '{}?code={}&state={}'.format(
                    t.client_uri,
                    t.client_code,
                    t.client_state,
                    )
            status = '302 Found'

        headers.append(('Location', uri),)

    elif env['PATH_INFO'] == '/token':
        if env['CONTENT_TYPE'] != 'application/x-www-form-urlencoded':
            raise ValueError('Invalid content')

        d = env['wsgi.input'].read()
        q = parse_qs(d.decode('utf-8'))
        logg.debug('input data {} {}'.format(d, q))
        if q['grant_type'][0] != 'authorization_code':
            raise ValueError('Invalid grant type')

        c = q['code'][0]
        r = q['redirect_uri'][0]

        t = OauthToken.get_by_code(c)
        if t == None:
            raise ValueError('Invalid code')

        if t.client_uri != r:
            raise ValueError('Client URI mismatch')

        session = ExampleTokenHandler.sessions[auth_result[1]]

        response_data = {
            'access_token': session.auth.hex(),
            'token_type': 'Bearer',
            'expires_in': int(session.auth_expire - time.time()),
            'scope': '',
                }
        response_data_json = json.dumps(response_data)
        content = response_data_json.encode('utf-8')
        headers.append(('Content-Type', 'application/json',))

    elif env['PATH_INFO'] == '/validate/acl':
        headers.append(('Content-Type', 'text/plain, charset=UTF8',))
        v = ExampleValidateHandler().load(auth_result[1])
        content = v.encode('utf-8')

    else:
        raise NotImplementedError
    
    return (status, headers, content)


def do_noauth(env, authenticator):
    h = []
    for m in authenticator.method():
        if m != None:
            h.append(('WWW-Authenticate', m,))
    if len(h) == 0:
        s = '403 No access'
    else:
        s = '401 Unauthorized'
    return(s, h, [b''])


# TODO: content-length filter
def application(env, start_response):

    for k in env.keys():
        logg.debug('{}: {}'.format(k, env[k]))

    # handle authentication
    authenticator = None
    try:
        authenticator = do_auth(env)
    except CredentialsError as e:
        start_response('400 ' + str(e), [])
        return [b'']
    except FileNotFoundError:
        start_response('404 Not found', [])
        return [b'']

    auth_result = authenticator.check()
    logg.debug('auth result {}'.format(auth_result))
    if not auth_result:
        (s, h, c) = do_noauth(env, authenticator)
        start_response(s, h)
        return [c]


    # generate content
    try:
        (s, h, c) = do_content(auth_result, env) 
        start_response(s, h)
        return [c]
    except NotImplementedError:
        logg.error('valid auth path has no content')
        start_response('500 Internal server error', [])
        return [b'']
    #except Exception as e:
    #    logg.error(e)
    #    start_response('500 Internal server error', [])
    #    return [b'']
