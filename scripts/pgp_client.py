import sys
import logging
import os

import urllib
import urllib.request
import argparse

from cic_auth.client.pgp import PGPClientSession

logging.basicConfig(level=logging.DEBUG)
logg = logging.getLogger()

url = sys.argv[1]

script_dir = os.path.realpath(os.path.dirname(__file__))

#pk_export_file = os.environ.get('GNUPGHOME', os.path.join(script_dir, '..', 'tests', 'testdata', 'pgp', 'privatekeys.asc'))
#pk_export_file = "/home/lash/src/ext/cic/grassrootseconomics/cic-auth-proxy/meta/examples/privatekey.asc"
pk_export_file = "/home/lash/src/ext/cic/grassrootseconomics/cic-auth-proxy/meta/tests/them.priv.asc"
#pk_export_file = "/home/lash/tmp/privatekey.asc"
f = open(pk_export_file, 'r')
pk_export = f.read()
f.close()

#Create Handler
handler = PGPClientSession(url, private_key=pk_export, passphrase='tralala')

# create "opener" (OpenerDirector instance)
opener = urllib.request.build_opener(handler)

# use the opener to fetch a URL
r = opener.open(url)
print(r)
r = opener.open(url)
print(r)

#for i in range(10):
#    r = opener.open(url)
#    print(r)
