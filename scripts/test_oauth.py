import sys
import urllib
from urllib import request
from urllib.parse import urlparse, parse_qs, urlencode
import uuid
import http
import base64
import json

client_id = sys.argv[1]
client_secret = 'bar'
state = uuid.uuid4().hex
redirect_uri = 'http://localhost:5554'

class MyHTTPRedirectHandler(urllib.request.HTTPRedirectHandler):
    def http_error_302(self, req, fp, code, msg, headers):
        return headers 
        #return urllib.request.HTTPRedirectHandler.http_error_302(self, req, fp, code, msg, headers)

    http_error_301 = http_error_303 = http_error_307 = http_error_302

opener = urllib.request.build_opener(MyHTTPRedirectHandler)
urllib.request.install_opener(opener)



req = request.Request('http://localhost:5555/authorize?client_id={}&response_type=code&state={}&redirect_uri={}'.format(
    client_id,
    state,
    redirect_uri,
    ),
)
h = urllib.request.urlopen(
        req,
        )
c = h['Set-Cookie']


req = request.Request('http://localhost:5555{}'.format(h['Location']))
b = base64.b64encode(b'lash:tralala')
req.add_header('Cookie', c)
req.add_header('Authorization', 'Basic {}'.format(b.decode('utf-8')))
h = urllib.request.urlopen(
        req,
        )
u = urlparse(h['Location'])
q = parse_qs(u.query)
print(q)

if q['state'][0] != state:
    ValueError('state {}'.format(q['state'][0]))


client_auth = '{}:{}'.format(client_id, client_secret)
b = base64.b64encode(client_auth.encode('utf-8'))
req = request.Request('http://localhost:5555/token')
req.add_header('Content-Type', 'application/x-www-form-urlencoded')
req.add_header('Authorization', 'Basic {}'.format(b.decode('utf-8')))
data = {
    'grant_type': 'authorization_code',
    'code': q['code'][0],
    'redirect_uri': redirect_uri,
    }
data_q = urlencode(data)
res = urllib.request.urlopen(req, data_q.encode('utf-8'))

data_r = json.load(res)
print('auth token {}'.format(data_r['access_token']))


req = request.Request('http://localhost:5555/validate/acl')
req.add_header('Authorization', 'Bearer {}'.format(data_r['access_token']))
res = urllib.request.urlopen(req, data_q.encode('utf-8'))
content = res.read()
print(content.decode('utf-8'))
