# standard imports
import unittest
import os
import logging
import tempfile
import shutil

# local imports
from usumbufu.retrieve import Fetcher
from usumbufu.filter.pgp import PGPFilter

script_dir = os.path.realpath(os.path.dirname(__file__))

logg = logging.getLogger(__name__)
logging.getLogger('gnupg').setLevel(logging.WARNING)


class KeyBundleFetcher(Fetcher):

    def __init__(self, directory):
        self.directory = directory


    def get(self, v):
        f = open(os.path.join(self.directory, v), 'rb')
        r = f.read()
        f.close()
        return r


class TestBaseAuth(unittest.TestCase):

    def setUp(self):
        self.data_dir = os.path.join(script_dir, 'testdata')


    def tearDown(self):
        pass


class TestPGPBaseAuth(TestBaseAuth):
        
    def setUp(self):
        super(TestPGPBaseAuth, self).setUp()
        self.pgp_dir = os.path.join(self.data_dir, 'pgp')
        self.pgp_home_dir = tempfile.mkdtemp()
        self.key_fetcher = KeyBundleFetcher(self.pgp_dir, )
        self.pgp_decoder = PGPFilter(['860F711EBC3196FA70E86FF700F3F3286541666B'], self.key_fetcher, gnupg_home=self.pgp_home_dir)


    def tearDown(self):
        shutil.rmtree(self.pgp_home_dir)
        super(TestPGPBaseAuth, self).tearDown()
