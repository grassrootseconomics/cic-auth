# standard imports
import logging
import tempfile
import base64

# extended imports
import gnupg
from http_hoba_auth import Hoba

# local imports
from usumbufu.client.base import (
        ClientSession,
        challenge_nonce,
    )
from usumbufu.client.util import (
        get_auth_header_parts,
        get_header_parts,
        )

logg = logging.getLogger(__name__)


class PGPClientSession:

    def __init__(self, origin, private_key=None, fingerprint=None, passphrase=None, gpg_dir=None, token_store=None):
        self.origin = origin
        actual_gpg_dir = gpg_dir
        if actual_gpg_dir == None:
            actual_gpg_dir = tempfile.mkdtemp()
        logg.info('using gpg dir {}'.format(actual_gpg_dir))
        self.gpg = gnupg.GPG(gnupghome=actual_gpg_dir)
        self.fingerprint = None
        if private_key != None:
            import_result = self.gpg.import_keys(private_key)
            self.fingerprint = import_result.results[0]['fingerprint']
            if import_result.sec_read == 0:
                raise ValueError('Export bundle contained no private keys')
            elif gpg_dir == None and import_result.sec_imported > 1:
                logg.warning('multiple private keys found. key with fingerprint {} will be used to sign challenges'.format(self.fingerprint))
        elif fingerprint != None:
            self.fingerprint = fingerprint
            #NotImplementedError('currently only works with passed private key export blobs')
        self.passphrase = passphrase


    def hoba_kid(self, encoding='base64'):
        kid = None
        #if encoding == 'base64':
        #    b = bytes.fromhex(self.fingerprint)
        #    kid = base64.b64encode(b)
        kid = bytes.fromhex(self.fingerprint)
        return kid 


    def process_auth_request(self, request):
        return request
        
 
    def process_auth_challenge(self, header, encoding='base64', method='HOBA'):
        if method != 'HOBA':
            logg.error('only HOBA implemented for pgp handler client, got {}'.format(method))
            return None

        o = get_header_parts(header)
        logg.debug('oo {}'.format(o))
        c = o['challenge']
        if encoding == 'base64':
            c = base64.b64decode(o['challenge'])
        elif encoding != None:
            NotImplementedError(encoding)
       
        hoba = Hoba(self.origin, o['realm'])
        hoba.challenge = c
        hoba.nonce = challenge_nonce()
        hoba.kid = self.hoba_kid()
        hoba.alg = '969'
        
        plaintext = hoba.to_be_signed()

        r = self.gpg.sign(plaintext, passphrase=self.passphrase, detach=True)
        
        if encoding == 'base64':
            r = r.data

        hoba.signature = r
        return str(hoba)


    def __str__(self):
        return 'pgp'
