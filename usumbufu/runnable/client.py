# standard imports
import argparse
import os
import sys
import urllib.parse
import urllib.request
import logging

# external imports
import confini

# local impors
from usumbufu.client.base import (
        ClientSession,
        BaseTokenStore,
        )
from usumbufu.client.bearer import BearerClientSession
from usumbufu.client.config import apply_args
from usumbufu.error import AuthenticationError

logging.basicConfig(level=logging.WARNING)
logg = logging.getLogger()

script_dir = os.path.dirname(os.path.realpath(__file__))
base_config_dir = os.path.join(script_dir, '..', 'data', 'config')
default_token_dir = '/run/user/{}/usumbufu/client/.token'.format(os.getuid())

argparser = argparse.ArgumentParser('Usumbufu client')
argparser.add_argument('--pgp-key', dest='pgp_key', type=str, help='PGP key to use for HOBA signing')
argparser.add_argument('--pgp-dir', dest='pgp_dir', type=str, help='PGP keyring dir')
argparser.add_argument('--env-prefix', dest='env_prefix', type=str, help='Config override environment variable prefix')
argparser.add_argument('-c', type=str, help='Config override dir')
argparser.add_argument('-v', action='store_true', help='Be verbose')
argparser.add_argument('-vv', action='store_true', help='Be more verbose')
argparser.add_argument('--token-store', type=str, dest='token_store', default=default_token_dir, help='Directory to store tokens in')
argparser.add_argument('url', type=str, help='URL to open')
args = argparser.parse_args(sys.argv[1:])

if args.vv:
    logg.setLevel(logging.DEBUG)
elif args.v:
    logg.setLevel(logging.INFO)

config = confini.Config(base_config_dir, env_prefix=args.env_prefix, override_dirs=args.c)
extra_args = {
        'token_store': 'TOKEN_STORE',
        'pgp_key': 'PGP_KEY',
        'pgp_dir': 'PGP_DIR',
        'url': None,
        }
config.process()
apply_args(config, args, extra_args)
logg.debug('config loaded\n' + str(config))

os.makedirs(config.get('TOKEN_STORE'), exist_ok=True)

u = urllib.parse.urlsplit(config.get('_URL'))
origin = '{}://{}'.format(u.scheme, u.netloc)
logg.debug('using origin {}'.format(origin))

def main():
    token_store = BaseTokenStore(path=config.get('TOKEN_STORE'))
    session = ClientSession(origin, token_store=token_store)
    
    bearer_handler = BearerClientSession(origin, token_store=token_store)
    session.add_subhandler(bearer_handler)

    if config.get('PGP_KEY'):
        from usumbufu.client.pgp import PGPClientSession
        pgp_handler = PGPClientSession(origin, gpg_dir=config.get('PGP_DIR'), fingerprint=config.get('PGP_KEY'), passphrase=os.environ.get('PGP_PASSPHRASE'))
        session.add_subhandler(pgp_handler)

    opener = urllib.request.build_opener(session)
    req = urllib.request.Request(config.get('_URL'))
    try:
        r = opener.open(req)
    except AuthenticationError as e:
        sys.stderr.write('error: {}\n'.format(e))
        sys.exit(1)
    print(r.read().decode('utf-8'))


if __name__ == '__main__':
    main()
